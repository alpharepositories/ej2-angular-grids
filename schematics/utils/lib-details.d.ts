export declare const pkgName = "@syncfusion/ej2-angular-grids";
export declare const pkgVer = "^17.1.48";
export declare const moduleName = "GridModule, PagerModule";
export declare const themeVer = "~17.1.48";
